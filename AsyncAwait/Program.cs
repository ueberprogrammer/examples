﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class A
    {
        private int n;
        
        public A()
        {
            n = 5;
        }
    }
    class Program
    {
        private static async Task Sleep(int id)
        {
            Console.WriteLine($"start sleep {id}");
            await Task.Delay(2000);
            Console.WriteLine($"stop sleep {id}");

        }

        private static async Task MyAsyncMethod()
        {
            Console.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}] befor await");

            var task1 = Sleep(1);
            var task2 = Sleep(2);
            var task3 = Sleep(3);

            await Task.WhenAll(task1, task2, task3);

            Console.WriteLine($"[{Thread.CurrentThread.ManagedThreadId}] after await");
        }

        static void Main(string[] args)
        {
            ThreadPool.SetMaxThreads(100, 0);
            ThreadPool.SetMinThreads(100, 0);

            MyAsyncMethod().Wait();

            var a = new A();
        }
    }
}