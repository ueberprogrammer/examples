﻿// Decompiled with JetBrains decompiler
// Type: AsyncAwait.Program
// Assembly: AsyncAwait, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 5662BB52-4B8F-4803-9B64-1ECA30896F85
// Assembly location: C:\repository\examples\AsyncAwait\bin\Debug\AsyncAwait.exe
// Compiler-generated code is shown

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    internal class Program1
    {
        private static Task MyAsyncMethod()
        {
            GeneratedAsyncAwaitClass stateMachine = new GeneratedAsyncAwaitClass();
            stateMachine.generated_builder = AsyncTaskMethodBuilder.Create();
            stateMachine.generated_state = -1;
            stateMachine.generated_builder.Start<GeneratedAsyncAwaitClass>(ref stateMachine);
            return stateMachine.generated_builder.Task;
        }


        [CompilerGenerated]
        private sealed class GeneratedAsyncAwaitClass : IAsyncStateMachine
        {
            public int generated_state;
            public AsyncTaskMethodBuilder generated_builder;
            private TaskAwaiter generated_awaiter;


            void IAsyncStateMachine.MoveNext()
            {
                int num1 = this.generated_state;
                try
                {
                    TaskAwaiter awaiter;
                    int num2;
                    if (num1 != 0)
                    {
                        Console.WriteLine(string.Format("[{0}] befor await", (object)Thread.CurrentThread.ManagedThreadId));
                        awaiter = Task.CompletedTask.GetAwaiter();
                        if (!awaiter.IsCompleted)
                        {
                            this.generated_state = num2 = 0;
                            this.generated_awaiter = awaiter;
                            GeneratedAsyncAwaitClass stateMachine = this;
                            this.generated_builder.AwaitUnsafeOnCompleted<TaskAwaiter, GeneratedAsyncAwaitClass>(ref awaiter, ref stateMachine);
                            return;
                        }
                    }
                    else
                    {
                        awaiter = this.generated_awaiter;
                        this.generated_awaiter = new TaskAwaiter();
                        this.generated_state = num2 = -1;
                    }
                    awaiter.GetResult();
                    Console.WriteLine(string.Format("[{0}] after await", (object)Thread.CurrentThread.ManagedThreadId));
                }
                catch (Exception ex)
                {
                    this.generated_state = -2;
                    this.generated_builder.SetException(ex);
                    return;
                }
                this.generated_state = -2;
                this.generated_builder.SetResult();
            }

            [DebuggerHidden]
            void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
            {
            }
        }
    }
}
