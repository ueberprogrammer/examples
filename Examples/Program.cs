﻿using System;
using System.Collections.Generic;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {

            HashSet<int> set = new HashSet<int>();

            set.Add(1);
            set.Add(4);
            set.Add(14);
            set.Add(4);

            foreach (var s in set)
            {
                Console.WriteLine(s);
            }

            var list = new List<int>();

            for(var i = 0; i < 30; i++)
                list.Add(i);

            Console.WriteLine("This is a test");
        }
    }
}
